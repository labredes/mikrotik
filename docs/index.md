#Mikrotik

Sitio web oficial: [mikrotik.com](http://www.mikrotik.com) 

Tutorial oficial: [wiki.mikrotik.com](http://wiki.mikrotik.com) 

Nombre de los sistemas operativos:

* RouterOS: OS para gestión de routers.

* SwitchOS: OS para gestión de switches.

Este tutorial desarrolla los principales comandos de configuración de RouterOS.
