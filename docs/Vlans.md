## Creación de interfaces VLAN

La creación y asignación de una vlan se realiza desde la sección **interface vlan**:

```bash
/interface vlan add vlan-id="#v" name="nombre" interface="interfaz"
```

Donde: ||
-- | --
**#v** = número de vlan | **nombre** = nombre asignado a la vlan |
**interfaz** = nombre de la interfaz asociada a la vlan ||

## Asignación de dirección IP a una VLAN

La asignación de una dirección IP a una vlan es equivalente a cuando se configura la IP a una interfaz:

```bash
/ip address add address="x.x.x.x"/"y" interface="#v"
```

Donde: | |
-- | -- |
**x.x.x.x** = Dirección IP | **y** = tamaño (en decimal) de la máscara de red |
**#v** = número de vlan ||
