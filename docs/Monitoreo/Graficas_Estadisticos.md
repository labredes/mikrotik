Gráficas y Estadísticos

## Gráfica de cpu, ram y disco:

```
/tool graphing resource add allow-address=0.0.0.0/0 store-on-disk=yes
```


## Habilitar estadísticos de queues:
```
/tool graphing queue add allow-address=0.0.0.0/0 store-on-disk=yes
```
