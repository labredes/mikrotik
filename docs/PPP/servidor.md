# PPP - servidor PPPoE

La configuración de un servidor PPPoE presenta los siguientes pasos:

* Creación de perfiles de usuario: Por medio de perfiles se puede administrar distinta configuración a un conjunto de usuarios, como pueden ser parámetros de red o velocidades de los enlaces.

* Carga de clientes

* Configuración de una interfaz por la que se va a brindar el servicio.

## Administración de clientes

### Carga de perfil de clientes

Para esta configuración, es posible asignar una dirección IP en forma estática a cada cliente o reservar un pool de direcciones para que se asigne una IP de un rango en particular (<a href="https://labredes.gitlab.io/mikrotik/Pool_de_direcciones/" target="_blank">enlace pool de direcciones</a>).

```bash
/ppp profile add name="n" local-address="x.x.x.x" remote-address="PoolPPP" dns-server="d.d.d.d" rate-limit="u [k/M/G]"/"d [k/M/G]" only-one="o"
```

Donde: | |
-- | --
**n** = Nombre del perfil | **x.x.x.x** = Dirección IP que el cliente usará como puerta de enlace
**PoolPPP** = Pool de direcciones reservado con anterioridad  | **d.d.d.d** = dirección IP del servidor DNS
**u** = Límite de velocidad de subida indicando la unidad [k/M/G]| **d** = Límite de velocidad de bajada indicando la unidad [k/M/G]
**o** = (yes/no; default = no) indica si se permite sólo una sesión ppp por cada secret que use este perfil |

### Carga de clientes PPPoE

Por cada cliente es necesario generar la asociación nombre de usuario, contraseña y perfil de usuario:

```bash
/ppp secret add name="u" password="p" service=pppoe profile="n"
```

Donde: | | |
-- | -- | --
**u** = nombre de usuario | **p** = contraseña | **n** = nombre del perfil


### Visualización de clientes cargados al sistema

```bash
/ppp secret print
Flags: X - disabled
#     NAME        SERVICE CALLER-ID     PASSWORD       PROFILE     REMOTE-ADDRESS
"#"   "usuario"   "servicio"            "contraseña"   "perfil"    "ip"    
```

Donde: | |
-- | -- 
**#** = número de línea  | **usuario** = nombre de usuario
**servicio** = nombre del servidor | **contraseña** = contraseña de usuario
**perfil** = perfil de usuario asignado | **ip** = dirección de IP estática asignada

## Asignación del servicio a una interface

```bash
/interface pppoe-server server add service-name="x" interface="y" one-session-per-host="o" disabled=no
```

Donde: | |
-- | -- |
**x** = Nombre del Servidor | **y** = Nombre de interfaz donde funciona el servidor |
**o** = (yes/no; default = no) permite una sóla sesión por dispositivo (dirección MAC). Si se intenta establecer una nueva sesión, la anterior se cierra.

## Visualización del funcionamiento

### Lectura de log

Mediante el log se pueden determinar problemas, por ejemplo si un usuario intenta identificarse y tiene un error en su contraseña.

```bash
/log print
```
### Visualización de clientes conectados

Se puede conocer los clientes que se encuentan registrados en el sistema con el siguiente comando: