# Conexión por consola

## Tipos de cables

| Figura 1: Cables DB9 a DB9 | Figura 2: Cable adaptador usb a DB9 |
| :-: | :-: |
| ![](imagenes/DB9aDB9.jpg)| ![](imagenes/USBaDB9.jpg) |

## Parámetros de configuración

| Figura 3: ventana inicial | Figura 4: parámetros de conexión serial |
| :-: | :-: |
| ![](imagenes/putty1.png)| ![](imagenes/putty2.png) |

Para la conexión serial, dependiendo el tipo de cable, es la configuración que se ha de usar en el argumento "Serial Line":

| Tipo de cable | Serial Line | 
| :-: | :-: |
| DB9 a DB9 (figura 1) | /dev/tty**S**0 |
| USB a DB9 (figura 2) |  /dev/tty**USB**0 |

Dentro de "Serial", dependiendo el modelo del equipo, se deben indicar los siguientes parámetros:

| Parámetro | Excepto modelo 230 | Modelo 230 |
| :-: | :-: | :-: |
| Speed | 115200 | 9600 |
| Flow control | None | RTS/CTS |
